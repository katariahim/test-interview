package interviewAPITest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PaytmInsiderGetAPITest {
	
	JSONArray upcomingMoviesArray;
	CloseableHttpResponse closeableHttpResponse;
	ArrayList<String> moviesListContentAvailableAszero = new ArrayList<String>();
	Set<String> paytmMovieCodes = new HashSet<String>();

	@BeforeTest
	public void getCallForUpcomingMoviesAndSetItInJsonArray() throws ClientProtocolException, IOException, JSONException, ParseException, InvalidFormatException
	{
		
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet("https://apiproxy.paytm.com/v2/movies/upcoming"); // creates a Get request
		closeableHttpResponse = httpclient.execute(httpget); // hit the get URl and get response
		String responseString = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8"); //To get the entity to response JSON
		JSONObject responseJSON = new JSONObject(responseString); 
		
		System.out.println("Reponse Json Received is :"+responseJSON);
		System.out.println("Lets Test Key"+responseJSON.getJSONArray("keys"));
		
		upcomingMoviesArray = responseJSON.getJSONArray("upcomingMovieData");
		
	}
	
	@Test
	public void verifyRespnseJson()
	{
		//Status Code validation done
		int responseStatus = closeableHttpResponse.getStatusLine().getStatusCode();
		System.out.println("Reponse Status Received is :"+responseStatus);
		Assert.assertEquals(responseStatus, 200);
	}
	
	@Test
	public void verifyJpgFormatInMoviePosterURL() throws InvalidFormatException, IOException
	{
		for(int i=0; i<upcomingMoviesArray.length(); i++)
		{
			//Checking that the movie Poster Url for each movies have ​.jpg​ format
			Assert.assertTrue(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("moviePosterUrl")).endsWith(".jpg"));
		}
	}
	
	@Test
	public void verifyLanguageUsedInUpcomingMovies()
	{
		for(int i=0; i<upcomingMoviesArray.length(); i++)
		{	
			System.out.println("Movie Poster URL is"+upcomingMoviesArray.getJSONObject(i).get("language"));	
			//Checking that there is only 1 language for each movies
			Assert.assertFalse(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("language")).contains(","));
			Assert.assertFalse(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("language")).contains(" "));
			Assert.assertFalse(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("language")).equals(null));
		}
	}
	
	@Test
	public void storeMoviesWithContentAvailableAszeroInExcelSheet() throws Exception
	{
		FileInputStream fileInputStream = new FileInputStream("./ExcelSheet/ExcelSheetToEnterData.xlsx");
		Workbook excelWorkbook = WorkbookFactory.create(fileInputStream);
		int numberOfSheets = excelWorkbook.getNumberOfSheets();
		if(numberOfSheets>=1)
		{
		for(int i=0; i<=numberOfSheets-1; i++)
	    {
	    	if(excelWorkbook.getSheetName(i).equals("MoviesWithContentAvailablezero"))
	    	{
	    		excelWorkbook.removeSheetAt(i);
	    	}
	    }
		}
	    Sheet excelSheet = excelWorkbook.createSheet("MoviesWithContentAvailablezero");
	    //int lastRow = excelSheet.getLastRowNum();
	    int rowCount = 0;
		for(int i=0; i<upcomingMoviesArray.length(); i++)
		{
			
			if(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("isContentAvailable")).equals("0"))
			{
				String movieName = String.valueOf(upcomingMoviesArray.getJSONObject(i).get("movie_name"));
				System.out.println("Movie name with is Content available is:"+movieName);
				moviesListContentAvailableAszero.add(movieName);
				Row row = excelSheet.createRow(rowCount++);
				Cell cell = row.createCell(0);
				cell.setCellValue(movieName);
				System.out.println("Row is :"+rowCount);
			}
			
			System.out.println("List observed is :"+moviesListContentAvailableAszero);
		}
		fileInputStream.close();
		 
         FileOutputStream fileOutputStream = new FileOutputStream("./ExcelSheet/ExcelSheetToEnterData.xlsx");
         excelWorkbook.write(fileOutputStream);
         fileInputStream.close();
         fileOutputStream.close();
	}
	
	@Test
	public void verifyEachUpcomingMovieReleaseDateshouldNotBeBeforeTodaysDate() throws ParseException
	{
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String todaysDate= dateFormat.format(date);
		Date dateToday = dateFormat.parse(todaysDate);
		for(int i=0; i<upcomingMoviesArray.length(); i++)
		{
			//checking that the dates for each movies is before Today's Date
			if(upcomingMoviesArray.getJSONObject(i).get("releaseDate").equals(null))
			{
			System.out.println("Movie Date with null is : "+String.valueOf(upcomingMoviesArray.getJSONObject(i).get("movie_name")));
			}
			Date movieReleaseDate = null;
			try {
				movieReleaseDate = dateFormat.parse(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("releaseDate")));
			}
			catch(ParseException e)
			{
				System.out.println("Failing due to null value.");
				Assert.fail("Failing due to null value.");
			}
			Assert.assertTrue(movieReleaseDate.after(dateToday));
		}
	}
	
	@Test
	public void verifyEachPaytmMovieCodeIsUnique()
	{
		for(int i=0; i<upcomingMoviesArray.length(); i++)
		{
			//Checking that the paytm Movie Code for each movies is unique(add method of Set will return if it comes duplicate)
			Assert.assertTrue(paytmMovieCodes.add(String.valueOf(upcomingMoviesArray.getJSONObject(i).get("paytmMovieCode"))));
		}
	}
}
